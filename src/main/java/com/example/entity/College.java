package com.example.entity;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.List;

@Entity
public class College implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @OneToMany(mappedBy = "college")
    private List<Student> students;
    @OneToMany(mappedBy = "college")
    private List<Teacher> teachers;
}
